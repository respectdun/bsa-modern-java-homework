package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {

	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null)
			return 0;

		int maxDepth = 1;
		for (Department department : rootDepartment.subDepartments) {
			int depthOfDepartment = calculateMaxDepth(department) + 1;
			if (depthOfDepartment > maxDepth)
				maxDepth = depthOfDepartment;

		}

		return maxDepth;
	}

}
