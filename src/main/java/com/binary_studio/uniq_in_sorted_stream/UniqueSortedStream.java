package com.binary_studio.uniq_in_sorted_stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}
	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		List<Row<T>> uniqFilteredData = stream.filter(distinctByKey(Row::getPrimaryId))
				.collect(Collectors.toCollection(ArrayList::new));

		return convertListToStream(uniqFilteredData);

	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {

		Map<Object, Boolean> seen = new ConcurrentHashMap<>();
		return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}

	private static <T> Stream<T> convertListToStream(List<T> list) {
		return list.stream();
	}

}
